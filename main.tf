#
# DO NOT DELETE THESE LINES UNTIL INSTRUCTED TO!
#
# Your AMI ID is:
#
#     ami-08a5419d45846dbc5
#
# Your subnet ID is:
#
#     subnet-0e424a5e8d6c864c0
#
# Your VPC security group ID is:
#
#     sg-0d278f1e94e76c5bb
#
# Your Identity is:
#
#     terraform-training-cobra
#

variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "us-east-1"
}

variable "ami" {}
variable "subnet_id" {}
variable "identity" {}
variable "vpc_security_group_ids" {
  type = list
}
provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region     = var.region
}

# resource aws_instance "jp" {
#   ami   = "ami-08a5419d45846dbc5"
#   instance_type = "t2.micro"
# }
resource aws_instance "jp1" {
#  ami   = "ami-08a5419d45846dbc5"
    ami = "ami-0ac80df6eff0e70b5"
  instance_type = "t2.small"
}
module "keypair" {
  source  = "mitchellh/dynamic-keys/aws"
  version = "2.0.0"
  path    = "${path.root}/keys"
  name    = "${var.identity}-key"
}
module "server" {
  source = "./server"

  ami                    = var.ami
  subnet_id              = var.subnet_id
  vpc_security_group_ids = var.vpc_security_group_ids
  identity               = var.identity
  key_name               = module.keypair.key_name
  private_key            = module.keypair.private_key_pem
}
output "public_ip" {
  value = module.server.public_ip
}

output "public_dns" {
  value = module.server.public_dns
}